fun main(args: Array<String>) {
    FirebaseNotificationManager.sendChatNotification("johnDoe69", "Hey, how are you?")
    FirebaseNotificationManager.sendTimerNotification("Your workout is now over")
    FirebaseNotificationManager.sendAlertNotification("Your heart-rate is over 180BPM, take a break")
    FirebaseNotificationManager.sendLocationNotification(46.770069, 23.633862)
}