import okhttp3.ResponseBody
import org.pixsee.fcm.Message
import org.pixsee.fcm.Sender
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object FirebaseNotificationManager {
    private const val SENDER_ID = "1063365157783"
    private const val LEGACY_SERVER_KEY = "AIzaSyAigHrQubnVMbwCBvASPT8bYCbAY2jj7ag"

    private const val EMULATOR_TOKEN = "fIzYTP-q_W0:APA91bFmcyEUoci_sNMlcqF_3zYSa2W8ixqScMB8pRIivsXNvslF8t_fyle-e2Q_YQ40I8-tzmkr3Xg0OwvKqMudzgRiZOKHyZ5Met5w3r1-eY2dvSTKQSCuQ2phQ6NBkvhKuUieUrFp"

    // Only seems to work with the "Legacy server key" and not with "Sender id"
    private val messageSender by lazy { Sender(LEGACY_SERVER_KEY) }

    fun sendAlertNotification(alertMessage: String) {
        getEmptyMessage("alert")
                .addData("alertMessage", alertMessage)
                .build()
                .send()
    }

    fun sendTimerNotification(timerMessage: String) {
        getEmptyMessage("timer")
                .addData("timerMessage", timerMessage)
                .build()
                .send()
    }

    fun sendChatNotification(senderUsername: String, message: String) {
        getEmptyMessage("chat")
                .addData("senderUsername", senderUsername)
                .addData("message", message)
                .build()
                .send()
    }

    fun sendLocationNotification(latitude: Double, longitude: Double) {
        getEmptyMessage("location")
                .addData("latitude", latitude)
                .addData("longitude", longitude)
                .build()
                .send()
    }

    private fun Message.send() {
        messageSender.send(this, FirebaseNotificationManager.FirebaseInteractionCallback())
    }

    private fun getEmptyMessage(notificationType: String) = Message.MessageBuilder().toToken(EMULATOR_TOKEN).addData("notificationType", notificationType)

    private class FirebaseInteractionCallback : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>, throwable: Throwable) {
            println("Failure")
            throwable.printStackTrace()
        }

        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
            if (response.isSuccessful) {
                println("Received successful response")
            } else {
                println("Unsuccessful response. Status code: ${response.code()}")
            }

        }
    }
}